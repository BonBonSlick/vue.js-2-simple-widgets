import Vue from 'vue'
import App from './App.vue'

// Global variable
Vue.prototype.WEATHER_API_SECRET = '35cdf36cfbcf7b82cefb2fe3a7a1c801'

/* eslint-disable-next-line no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
